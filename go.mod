module citools

go 1.18

require (
	github.com/aws/aws-sdk-go v1.18.6
	github.com/pkg/errors v0.8.1
	github.com/urfave/cli v1.20.0
	gopkg.in/src-d/go-git.v4 v4.10.0
)

require (
	github.com/emirpasic/gods v1.9.0 // indirect
	github.com/jbenet/go-context v0.0.0-20150711004518-d14ea06fba99 // indirect
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af // indirect
	github.com/kevinburke/ssh_config v0.0.0-20180830205328-81db2a75821e // indirect
	github.com/mitchellh/go-homedir v1.0.0 // indirect
	github.com/pelletier/go-buffruneio v0.2.0 // indirect
	github.com/sergi/go-diff v1.0.0 // indirect
	github.com/src-d/gcfg v1.4.0 // indirect
	github.com/xanzy/ssh-agent v0.2.0 // indirect
	golang.org/x/crypto v0.0.0-20180904163835-0709b304e793 // indirect
	golang.org/x/net v0.0.0-20180906233101-161cd47e91fd // indirect
	golang.org/x/sys v0.0.0-20180903190138-2b024373dcd9 // indirect
	gopkg.in/src-d/go-billy.v4 v4.2.1 // indirect
	gopkg.in/warnings.v0 v0.1.2 // indirect
)
