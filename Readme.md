# citools
[![pipeline status](https://gitlab.com/pgmtc/citools/badges/master/pipeline.svg)](https://gitlab.com/pgmtc/citools/commits/master)

The purpose of this cli program is to provide a set of tools which make easier to achieve frequently used steps, without extensive configuration or dependencies.

- Do you need to copy file to s3 but you don't want to install python, pip and AWS cli? \
Use `ci s3-upload` instead.

- Do you want to generate and publish a homebrew formula as a part of your release without having git installed? \
Use `ci brew-publish` instead.

#### Installation
##### Mac OS:
1. Add pgmtc tap to your repo list\
`brew tap pgmtc/repo`
2. Install citools \
`brew update && brew install ci`
##### Other OS:
work in progress
#### Usage

##### brew-publish:
Publishes to brew repository example (dry run)
```bash
ci brew-publish \
    --dry-run                                           # Creates formula, but does not push to the repository
    --repo-url https://github.com/some/homebrew-repo \  # URL of the homebrew repository / tap, also sourced from BREW_REPO_URL env.variable
    --repo-token [personal_token] \                     # homebrew repository personal token, also sourced from BREW_REPO_TOKEN env.variable
    --name testpkg \                                    # name of the package
    --version 0.0.1 \                                   # version of the package
    --description "description of the package" \        # formula description
    --home-page http://homepage.com \                   # homepage whcih goes to formula
    --download-url=http://google.com/something.tar.gz \ # release package location whcih includes binary
    --commit-message "testpkg release 0.0.1" \          # publish commit message 
    --commit-author "ci/cd" \                           # publish commit author
    --commit-email cicd@homepage.com \                  # publish commit email
    --sha256 [sha_of_download_url_file] \               # Provide this
    --file package.tar.gz                               # or this to generate SHA256
```

##### s3-upload
Uploads file to s3 bucket
```bash
 ci s3-upload \
    --source package.tar.gz \                           # local file to upload
    --bucket bucket-name \                              # destination bucket
    --target package.tar.gz \                           # path in destination bucket
    --access-key [AWS_ACCESS_KEY] \                     # aws access key, also sourced from AWS_ACCESS_KEY_ID env.variable
    --secret-key [AWS_SECRET_KEY] \                     # aws secret key, also sourced from AWS_SECRET_ACCESS_KEY env.variable
    --region eu-west-2                                  # bucket region, also sourced from AWS_DEFAULT_REGION env.variable
```
Uploads ./dist/ directory to s3 bucket
```bash
 ci s3-upload \
    --source ./dist \                                   # local dir to upload
    --bucket bucket-name \                              # destination bucket
    --target dist \                                     # dir path in the bucket
    --access-key [AWS_ACCESS_KEY] \                     # aws access key, also sourced from AWS_ACCESS_KEY_ID env.variable
    --secret-key [AWS_SECRET_KEY] \                     # aws secret key, also sourced from AWS_SECRET_ACCESS_KEY env.variable
    --region eu-west-2                                  # bucket region, also sourced from AWS_DEFAULT_REGION env.variable
```