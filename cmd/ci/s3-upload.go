package main

import (
	"citools/pkg/s3"
	"fmt"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
	"strings"
)

var accessKey string
var secretKey string
var region string
var source string
var bucket string
var target string
var empty bool

// S3UploadFlags return list of flags for the command
var S3UploadFlags = []cli.Flag{
	cli.StringFlag{
		Name:        "access-key, a",
		Usage:       "aws access key",
		EnvVar:      "AWS_ACCESS_KEY_ID",
		Destination: &accessKey,
	},
	cli.StringFlag{
		Name:        "secret-key, k",
		Usage:       "aws secret key",
		EnvVar:      "AWS_SECRET_ACCESS_KEY",
		Destination: &secretKey,
	},
	cli.StringFlag{
		Name:        "region, r",
		Usage:       "aws region",
		EnvVar:      "AWS_DEFAULT_REGION",
		Destination: &region,
	},
	cli.StringFlag{
		Name:        "source, f",
		Usage:       "source file/dir",
		Destination: &source,
	},
	cli.StringFlag{
		Name:        "bucket, b",
		Usage:       "target bucket",
		Destination: &bucket,
	},
	cli.StringFlag{
		Name:        "target, t",
		Usage:       "target file/dir",
		Destination: &target,
	},
	cli.BoolFlag{
		Name:        "empty-bucket",
		Usage:       "empty bucket before uploading",
		Destination: &empty,
	},
}

// S3UploadAction represents main action of the command
func S3UploadAction(context *cli.Context) error {
	missing := checkS3CopyMandatoryParams()
	if len(missing) > 0 {
		cli.ShowSubcommandHelp(context)
		return errors.Errorf("ERROR:\n   missing %s\n", strings.Join(missing, ", "))
	}
	_, err := s3.Upload(accessKey, secretKey, region, source, bucket, target, empty)
	if err != nil {
		return err
	}
	fmt.Println("Done")
	//fmt.Printf("Uploaded files: \n\t%s\n", strings.Join(uploadedPaths, "\n\t"))
	return nil
}

func checkS3CopyMandatoryParams() []string {
	missing := []string{}

	if accessKey == "" {
		missing = append(missing, "access-key")
	}
	if secretKey == "" {
		missing = append(missing, "secret-key")
	}
	if region == "" {
		missing = append(missing, "region")
	}
	if source == "" {
		missing = append(missing, "source")
	}
	if bucket == "" {
		missing = append(missing, "bucket")
	}
	return missing
}
