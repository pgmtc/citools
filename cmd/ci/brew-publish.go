package main

import (
	"citools/pkg/brew"
	"crypto/sha256"
	"fmt"
	"github.com/pkg/errors"
	"github.com/urfave/cli"
	"io"
	"os"
	"strings"
)

var dryRun bool
var repoURL string
var repoToken string
var pkgName string
var pkgVersion string
var pkgDescription string
var pkgHomepage string
var pkgDownloadURL string
var pkgSha256 string
var commitMessage string
var commitAuthor string
var commitEmail string
var file string

// BrewPublishFlags return list of flags for the command
var BrewPublishFlags = []cli.Flag{
	cli.BoolFlag{
		Name:        "dry-run",
		Usage:       "do everything except pushing",
		Destination: &dryRun,
	},
	cli.StringFlag{
		Name:        "repo-url, u",
		Usage:       "url of the homebrew repository",
		EnvVar:      "BREW_REPO_URL",
		Destination: &repoURL,
	},
	cli.StringFlag{
		Name:        "repo-token",
		Usage:       "personal token of homebrew repository",
		EnvVar:      "BREW_REPO_TOKEN",
		Destination: &repoToken,
	},
	cli.StringFlag{
		Name:        "name",
		Usage:       "name of the package",
		Destination: &pkgName,
	},
	cli.StringFlag{
		Name:        "version",
		Usage:       "version of the package",
		Destination: &pkgVersion,
	},
	cli.StringFlag{
		Name:        "description",
		Usage:       "description of the package",
		Destination: &pkgDescription,
	},
	cli.StringFlag{
		Name:        "home-page",
		Usage:       "homepage of the package",
		Destination: &pkgHomepage,
	},
	cli.StringFlag{
		Name:        "download-url",
		Usage:       "download url of the package",
		Destination: &pkgDownloadURL,
	},
	cli.StringFlag{
		Name:        "sha256",
		Usage:       "sha256 url of the package file (provide --sha256 or --file)",
		Destination: &pkgSha256,
	},
	cli.StringFlag{
		Name:        "commit-message",
		Usage:       "commit message",
		Destination: &commitMessage,
	},
	cli.StringFlag{
		Name:        "commit-author",
		Usage:       "commit author",
		Destination: &commitAuthor,
	},
	cli.StringFlag{
		Name:        "commit-email",
		Usage:       "commit email",
		Destination: &commitEmail,
	},
	cli.StringFlag{
		Name:        "file, f",
		Usage:       "file refered by download url (provide --file or --sha256)",
		Destination: &file,
	},
}

// BrewPublishAction represents main action of the command
func BrewPublishAction(context *cli.Context) error {
	missing := checkBrewMandatoryParams()
	if len(missing) > 0 {
		cli.ShowSubcommandHelp(context)
		return errors.Errorf("ERROR:\n   missing %s\n", strings.Join(missing, ", "))
	}
	return publishPackage()
}

func checkBrewMandatoryParams() []string {
	missing := []string{}

	if repoURL == "" {
		missing = append(missing, "repo-url")
	}
	if repoToken == "" {
		missing = append(missing, "repo-token")
	}
	if pkgName == "" {
		missing = append(missing, "name")
	}
	if pkgVersion == "" {
		missing = append(missing, "version")
	}
	if pkgDescription == "" {
		missing = append(missing, "description")
	}
	if pkgHomepage == "" {
		missing = append(missing, "homepage")
	}
	if pkgDownloadURL == "" {
		missing = append(missing, "download-url")
	}
	if pkgSha256 == "" && file == "" {
		missing = append(missing, "sha256 or file")
	}
	return missing
}

func publishPackage() error {
	if pkgSha256 == "" && file != "" {
		fileSha, err := getFileSha256(file)
		if err != nil {
			return err
		}
		pkgSha256 = fileSha
		fmt.Printf("File SHA = %s", pkgSha256)
	}
	brw := brew.Repository{
		URL:   repoURL,
		Token: repoToken,
	}
	pkg := brew.Package{
		Name:        pkgName,
		Version:     pkgVersion,
		Description: pkgDescription,
		Homepage:    pkgHomepage,
		DownloadURL: pkgDownloadURL,
		SHA256:      pkgSha256,
	}

	cmt := brew.Commit{
		Message: commitMessage,
		Author:  commitAuthor,
		Email:   commitEmail,
	}

	return brew.Publish(dryRun, brw, pkg, cmt)
}

func getFileSha256(fileName string) (result string, resultErr error) {
	filePath := brew.ParsePath(fileName)
	if !brew.FileExists(filePath) {
		resultErr = errors.Errorf("File %s does not exist", filePath)
		return
	}
	f, err := os.Open(brew.ParsePath(file))
	if err != nil {
		resultErr = errors.Errorf("Unable to open file %s: %s", filePath, err.Error())
		return
	}
	defer f.Close()
	h := sha256.New()
	if _, err := io.Copy(h, f); err != nil {
		resultErr = errors.Errorf("Error when hashing: %s", err.Error())
	}
	result = fmt.Sprintf("%x", h.Sum(nil))
	return
}
