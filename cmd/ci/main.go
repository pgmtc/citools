package main

import (
	"github.com/urfave/cli"
	"log"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "ci"
	app.Usage = "set of tools useful for ci / cd"
	app.HideVersion = true
	app.Commands = []cli.Command{
		{
			Name:    "brew-publish",
			Aliases: []string{"bp"},
			Usage:   "publishes the package to the brew repository",
			Flags:   BrewPublishFlags,
			Action:  BrewPublishAction,
		},
		{
			Name:    "s3-upload",
			Aliases: []string{"s3"},
			Usage:   "uploads file/dir to s3",
			Flags:   S3UploadFlags,
			Action:  S3UploadAction,
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)

	}
}
