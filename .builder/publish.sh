#!/usr/bin/env bash
set -e
PKG="$1"
VERSION="$2"
PKG_NAME="$3"
PKG_DESC="$4"
PKG_HOMEPAGE="$5"
PKG_PATH="dist/$PKG"

echo "PKG = $PKG"
echo "VERSION = $VERSION"
echo "PKG_NAME = $PKG_NAME"
echo "PKG_DESC = $PKG_DESC"
echo "PKG_HOMEPAGE = $PKG_HOMEPAGE"
echo "PKG_PATH = $PKG_PATH"

SHA=$(sha256sum ${PKG_PATH} | awk '{ print $1 }')
S3_PATH="pgmtc-releases/$PKG_NAME/"
S3_PUBLIC_PATH="https://s3.eu-west-2.amazonaws.com/${S3_PATH}${PKG}"
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
BREW_GIT="git@github.com:pgmtc/homebrew-repo.git"
BREW_PATH="${SCRIPT_DIR}/.brew-repo"

aws s3 cp ${PKG_PATH} s3://${S3_PATH}
echo "Release available at: ${S3_PUBLIC_PATH}"

echo "Cloning homebrew repo"
rm -rf ${BREW_PATH}
git clone ${BREW_GIT} ${BREW_PATH}

echo "Generating formula ..."
FORMULA_FILENAME=${BREW_PATH}/Formula/${PKG_NAME}.rb
echo "FORMULA_FILENAME = $FORMULA_FILENAME"

cat <<EOT > ${FORMULA_FILENAME}
class ${PKG_NAME^} < Formula
  desc "${PKG_NAME} - ${PKG_DESC}"
  homepage "${PKG_HOMEPAGE}"
  url "${S3_PUBLIC_PATH}"
  version "${VERSION}"
  sha256 "${SHA}"

  def install
    bin.install "${PKG_NAME}"
  end
end
EOT

cd ${BREW_PATH}
git commit -a -m "release of citools ${VERSION}"
git push
