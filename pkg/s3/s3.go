package s3

import (
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/pkg/errors"
	"log"
	"os"
	"path/filepath"
)

// Upload copies local file / directory to s3
//  d3.Upload("ABCD", "DEFG", "eu-west-1", "local-source.txt", "somedir/target-source.txt")
func Upload(accessKeyID string, secretKeyID string, region string, srcFile string, dstBucket string, dstFile string, empty bool) (uploadedPaths []string, resultErr error) {
	fi, err := os.Stat(srcFile)
	if err != nil {
		resultErr = err
		return
	}

	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(region),
		Credentials: credentials.NewStaticCredentials(accessKeyID, secretKeyID, ""),
	})
	if err != nil {
		resultErr = fmt.Errorf("error when creating session: %s", err.Error())
		return
	}

	if fi.Mode().IsRegular() && dstFile == "" {
		resultErr = errors.Errorf("Source is a file, provide target as well")
		return
	}

	if empty {
		err := EmptyBucket(sess, dstBucket)
		if err != nil {
			resultErr = err
			return
		}
	}

	switch mode := fi.Mode(); {
	case mode.IsDir():
		return uploadDir(sess, srcFile, dstBucket, dstFile, empty)
	case mode.IsRegular():
		return uploadFile(sess, srcFile, dstBucket, dstFile, empty)
	}
	return
}

func uploadDir(sess *session.Session, srcDir string, dstBucket string, dstDir string, empty bool) (uploadedPaths []string, resultErr error) {
	_, fileList := mkDirAndFileList(srcDir)
	remoteDir := dstDir
	if dstDir != "" {
		remoteDir = remoteDir + "/"
	}
	for _, filePath := range fileList {
		s3Path, err := uploadFile(sess, filePath, dstBucket, remoteDir+filePath, empty)
		if err != nil {
			resultErr = err
			return
		}
		uploadedPaths = append(uploadedPaths, s3Path...)
	}
	return
}

func uploadFile(sess *session.Session, srcFile string, dstBucket string, dstFile string, empty bool) (uploadedPaths []string, resultErr error) {
	targetURL := fmt.Sprintf("https://s3.%s.amazonaws.com/%s/%s", *sess.Config.Region, dstBucket, dstFile)
	contentType := getContentType(srcFile)

	fmt.Printf("Uploading %s to %s as %s\n", srcFile, targetURL, contentType)
	file, err := os.Open(srcFile)
	if err != nil {
		resultErr = fmt.Errorf("unable to open source %s, %v", srcFile, err)
		return
	}
	defer file.Close()

	uploader := s3manager.NewUploader(sess)
	_, err = uploader.Upload(&s3manager.UploadInput{
		Bucket:      aws.String(dstBucket),
		Key:         aws.String(dstFile),
		Body:        file,
		ContentType: &contentType,
	})
	if err != nil {
		resultErr = fmt.Errorf("unable to upload %s to %s, %v", srcFile, dstBucket, err)
		return
	}
	uploadedPaths = []string{targetURL}
	return
}

// EmptyBucket removes all existing content from the given bucket
func EmptyBucket(sess *session.Session, bucket string) (resultErr error) {
	fmt.Printf("Emptying bucket: %s\n", bucket)
	svc := s3.New(sess)
	iter := s3manager.NewDeleteListIterator(svc, &s3.ListObjectsInput{
		Bucket: aws.String(bucket),
	})

	if err := s3manager.NewBatchDeleteWithClient(svc).Delete(aws.BackgroundContext(), iter); err != nil {
		resultErr = fmt.Errorf("unable to delete objects from bucket %q, %v", bucket, err)
		return
	}
	return
}

//EmptyBucket empties the Amazon S3 bucket
func mkDirAndFileList(dirPath string) (dirList []string, fileList []string) {
	err := filepath.Walk(dirPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			switch mode := info.Mode(); {
			case mode.IsDir():
				dirList = append(dirList, path)
			case mode.IsRegular():
				fileList = append(fileList, path)
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}
	return
}
