package brew

import (
	"fmt"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/transport/http"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"
)

// Publish creates / updates record in brew repository
func Publish(dryRun bool, brw Repository, pkg Package, cmt Commit) (resultErr error) {
	// Clone repository into local temp directory
	r, repoLocation, resultErr := cloneRepository(brw.URL, dryRun)
	if resultErr != nil {
		return
	}
	if !dryRun {
		defer os.RemoveAll(repoLocation)
	}

	// Generate brew formula
	pkgFileName := filepath.Join("Formula", pkg.Name+".rb")
	formulaContents := generateFormula(pkg)
	MkFile(filepath.Join(repoLocation, pkgFileName), formulaContents)

	// Commit changes
	resultErr = commitChanges(r, pkgFileName, cmt, dryRun)
	if resultErr != nil {
		return
	}
	// Push Changes
	if !dryRun {

		resultErr = pushChanges(r, brw.Token)
		if resultErr != nil {
			return
		}
	}
	fmt.Println("Done")
	return
}

func cloneRepository(url string, dryRun bool) (respository *git.Repository, repoLocation string, resultErr error) {
	fmt.Println("Checking out ... ")
	repoLocation = MkTempDir("")
	var progressWriter io.Writer
	if dryRun {
		progressWriter = os.Stdout
		fmt.Println("Repository location: ", repoLocation)
	}
	respository, err := git.PlainClone(repoLocation, false, &git.CloneOptions{
		URL:      url,
		Progress: progressWriter,
	})
	if err != nil {
		resultErr = err
	}
	return
}

func commitChanges(r *git.Repository, fileName string, cmt Commit, dryRun bool) (resultErr error) {
	fmt.Println("Commiting changes ...")
	w, resultErr := r.Worktree()
	if resultErr != nil {
		return
	}
	_, resultErr = w.Add(fileName)
	if resultErr != nil {
		return
	}
	status, resultErr := w.Status()
	if resultErr != nil {
		return
	}

	if dryRun {
		fmt.Println("Working tree changes:")
		fmt.Println(status)
	}

	commit, resultErr := w.Commit(cmt.Message, &git.CommitOptions{
		Author: &object.Signature{
			Name:  cmt.Author,
			Email: cmt.Email,
			When:  time.Now(),
		},
	})
	if resultErr != nil {
		return
	}
	obj, resultErr := r.CommitObject(commit)
	if resultErr != nil {
		return
	}
	if dryRun {
		fmt.Println("Commit object:")
		fmt.Println(obj)
	}
	return
}

func pushChanges(r *git.Repository, authToken string) (resultErr error) {
	fmt.Println("Pushing changes ...")
	resultErr = r.Push(&git.PushOptions{
		Auth: &http.BasicAuth{
			Username: "irrelevant", // yes, this can be anything except an empty string
			Password: authToken,
		},
	})
	return
}

func generateFormula(p Package) string {
	cmdName := strings.ToLower(p.Name)
	className := strings.Title(p.Name) // capitalizes first letter
	tpl := `
class %s < Formula
  desc %q
  homepage %q
  url %q
  version %q
  sha256 %q
  def install
    bin.install %q
  end
end`
	formula := fmt.Sprintf(tpl, className, p.Description, p.Homepage, p.DownloadURL, p.Version, p.SHA256, cmdName)
	return formula
}
