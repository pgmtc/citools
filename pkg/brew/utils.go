package brew

import (
	"io/ioutil"
	"os"
	"os/user"
	"path/filepath"
	"strings"
)

// ParsePath takes human readable path (relative or absolute, with ~) and returns it to absolute path
// Main motivation is to turn ~ to home directory
func ParsePath(path string) (result string) {
	if path == "." {
		path, _ = os.Getwd()
	}

	result = path

	if strings.Contains(path, "~") {
		usr, err := user.Current()
		if err == nil {
			result = strings.Replace(result, "~", usr.HomeDir, 1)
		}
	}

	if !strings.HasPrefix(result, "/") {
		currentDir, _ := os.Getwd()
		result = currentDir + "/" + result
	}
	return
}

// MkTempDir Creates a temporary directory and returns path to it
func MkTempDir(dirSuffix string) string {
	tmpDir, _ := ioutil.TempDir("", dirSuffix)
	return tmpDir
}

// MkTempFile Creates a temporary file with provided string content, returns path
func MkTempFile(fileName string, fileContents string) string {
	tmpDir := MkTempDir("")
	filePath := filepath.Join(tmpDir, fileName)
	return MkFile(filePath, fileContents)
}

// MkFile Creates a file with given content
func MkFile(path string, fileContents string) string {
	byteContent := []byte(fileContents)
	filePath := ParsePath(path)
	ioutil.WriteFile(filePath, byteContent, 0644)
	return filePath
}

// FileExists returns rue if the file exists
func FileExists(path string) bool {
	parsedPath := ParsePath(path)
	if _, err := os.Stat(parsedPath); err == nil {
		return true
	}
	return false
}

// ReadFile returns contents of the file. It uses ParsePath at the top of standard ioutil.ReadFile
func ReadFile(path string) ([]byte, error) {
	return ioutil.ReadFile(ParsePath(path))
}
