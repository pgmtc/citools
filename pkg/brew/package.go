package brew

// Package contains information about the published package
type Package struct {
	Name        string
	Version     string
	Description string
	Homepage    string
	DownloadURL string
	SHA256      string
}

// Repository contains information about the repository where the package is published to
type Repository struct {
	URL   string
	Token string
}

// Commit represents a message used when the package is published to the brew repository
type Commit struct {
	Message string
	Author  string
	Email   string
}
